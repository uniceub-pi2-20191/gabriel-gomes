import React, { Component } from 'react';
import { StyleSheet, View, TextInput, ScrollView  } from 'react-native';
import { Table, Row, Rows } from 'react-native-table-component';

import { createStackNavigator } from 'react-navigation';
 
export default class TablePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
          tableHead: ['Qnt', 'Produtos/Serviços', 'Valores'],
          tableData: [
            ['1', 'Concreto', 'R$ 89,10', ],
            ['3', 'Tintas', 'R$ 53,14', ],
            ['21', 'Telhas', 'R$ 340,60', ],
            ['67', 'Cimento', 'R$ 1100,00', ],
            ['4', 'Aço', 'R$ 80,95', ],
            ['2', 'Tubo Drenagem', 'R$ 47,69', ],
            ['1', 'Interruptor', 'R$ 20,27', ],
            ['14', 'Parafuso', 'R$ 13,30', ]
          ]
        }
      }
     
      render() {
        const state = this.state;
        return (
          <View style={styles.container}>
              <TextInput
              placeholder="Cliente:________________________________________"
              />
              <TextInput
              placeholder="Endereço:____________________ Tel:______________"
              />
              <ScrollView style={styles.dataWrapper}>
            <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
              <Row data={state.tableHead} style={styles.head} textStyle={styles.text}/>
              <Rows data={state.tableData} textStyle={styles.text}/>
            </Table>
            </ScrollView>
            <TextInput style={styles.place}
              placeholder="Cliente:___________ Sua Empresa:____________"
              />
          </View>
        )
      }
    }
     
    const styles = StyleSheet.create({
      container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
      head: { height: 40, backgroundColor: '#f1f8ff' },
      text: { margin: 6 },
      place: {
          marginTop: 20
      }
    });